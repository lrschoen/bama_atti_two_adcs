#include "Adafruit_TSL2591.h"
#include "Adafruit_TSL2591.cpp"

int main(){
    int address = 2591;
    Adafruit_TSL2591 tsl(address);
    tsl.enable();
    tsl.disable();
    return 0;
}
