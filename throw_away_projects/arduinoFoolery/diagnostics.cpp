#include "diagnostics.h"

using namespace ADCS;

bool diagnostics::run_eom_check()
{
	dynamics dynamics_instance;

	float t0 = 0.0f;
	float tf = 6.0f;
	float dt = 5e-2f;

	rk7_control<dynamics> rk;
	rk.gather_history = true;

	rk.set_model(&dynamics_instance);
	
	quaternion q_initial(z_axis, -90.0f);

	state_def initial_state_(q_initial, vec3(0.0f, 0.0f, to_rad(15.0f))); // pointing down inertial y axis, rotating positively around the z axis
	dynamics::state_t initial_state = initial_state_.map_to_array<dynamics::state_t>();

	vec3 constant_torque(0.0f, 0.0f, -0.00403f); // negative torque around the z axis
	dynamics::control_t constant_control = state_def::map_torques_to_array<dynamics::control_t>(dynamics_instance.m_actuators.cart_to_wheel(constant_torque));

	rk.generate_constant_control(t0, tf, constant_control, dt);
	rk.integrate(t0, tf, initial_state, dt);

	
	state_def final_state = state_def::map_from_array<dynamics::state_t>(rk.trajectory_history.back());

	std::vector<dynamics::state_t> rk_state_history = rk_history_to_vector_of_states(rk, &dynamics_instance.m_actuators);
	std::vector<dynamics::control_t> rk_control_history = rk_control_to_vector_of_states(rk, constant_control);
	std::vector<float> rk_time_history = rk_time_to_vector_of_floats(rk);

	vector_of_states_to_file<dynamics::state_t>(rk_state_history, "attitude_history.txt");
	vector_of_states_to_file<dynamics::control_t>(rk_control_history, "wheel_torque_history.txt");
	vector_of_floats_to_file(rk_time_history, "time.txt");
	

	/*if (
		final_state_cart.m_ang_pos(0)   < 0.002f  && 0.001f  < final_state_cart.m_ang_pos(0)   &&
		final_state_cart.m_ang_pos(1)   < -0.998f && -1.001f  < final_state_cart.m_ang_pos(1)   &&
		final_state_cart.m_ang_pos(2)   < 0.0001f && -0.0001f < final_state_cart.m_ang_pos(2)   &&
		final_state_cart.m_ang_vel(0)   < 0.0001f && -0.0001f < final_state_cart.m_ang_vel(0)   &&
		final_state_cart.m_ang_vel(1)   < 0.0001f && -0.0001f < final_state_cart.m_ang_vel(1)   &&
		final_state_cart.m_ang_vel(2)   < 0.001f  && -0.001f  < final_state_cart.m_ang_vel(2)   &&
		final_state.m_ang_mom_wheels(0) < 0.0001f && -0.0001f < final_state.m_ang_mom_wheels(0) &&
		final_state.m_ang_mom_wheels(1) < 0.0001f && -0.0001f < final_state.m_ang_mom_wheels(1) &&
		final_state.m_ang_mom_wheels(2) < 0.057f  && 0.056f   < final_state.m_ang_mom_wheels(2) &&
		final_state.m_ang_mom_wheels(3) < 0.0001f && -0.0001f < final_state.m_ang_mom_wheels(3)
		)
	{
		return true;
	}
	else
	{
		return false;
	}*/

	

	return true;
}

bool diagnostics::run_pid_check()
{
	pid pid_instance;

	pid_instance.set_desired_state(pid_instance.m_dynamics.ref_q);

	quaternion q_initial(vec3(1.0f, 1.0f, 1.0f), 170.0f);
	float degrees_per_second = to_rad(15.0f);
	state_def initial_state(q_initial, vec3(0.0f, 0.0f, degrees_per_second));
	pid_instance.m_dynamics.m_current_state = initial_state;

	pid_instance.m_dynamics.spin_up();

	pid_instance.set_gains_using_MOI(vec3(0.0f, 0.0f, 0.0f));
	while (pid_instance.m_elapsed_time < 10.0f)
	{
		pid_instance.sim_iteration();
	}

	pid_instance.set_gains_using_MOI(pid_instance.m_detumble_gains);
	while (pid_instance.m_elapsed_time < 10.0f + 30.0f)
	{
		pid_instance.sim_iteration();
	}

	pid_instance.purge_error();

	pid_instance.set_gains_using_MOI(pid_instance.m_slew_gains);
	while (pid_instance.m_elapsed_time < 10.0f + 30.0f + 110.0f)
	{
		pid_instance.sim_iteration();

	}

	

	vector_of_states_to_file<dynamics::state_t>(pid_instance.m_attitude_history, "attitude_history.txt");
	vector_of_floats_to_file(pid_instance.m_time_history, "time.txt");
	vector_of_states_to_file<dynamics::control_t>(pid_instance.m_wheel_torque_history, "wheel_torque_history.txt");

	return true;
}







std::vector<dynamics::state_t> diagnostics::rk_history_to_vector_of_states(rk7<dynamics> rk, actuators* a)
{
	int points = 500;
	if (points > rk.trajectory_history.size()) points = rk.trajectory_history.size();
	
	float increment = float(rk.trajectory_history.size()) / float(points);
	
	float ix_float = 0.0;
	int ix = 0;

	std::vector<dynamics::state_t> trajectory_history;

	dynamics::state_t state;

	for (int i = 0; i < points-1; i++)
	{
		
		for (int j = 0; j < 7; j++)
		{
			state[j] = rk.trajectory_history[ix][j];
		}

		std::array<float, 3> ang_mom_xyz_array;
		for (int j = 0; j < 3; j++)
		{
			ang_mom_xyz_array[j] = rk.trajectory_history[ix][7 + j];
		}
		vec3 ang_mom_xyz = vec3(ang_mom_xyz_array[0], ang_mom_xyz_array[1], ang_mom_xyz_array[2]);
		vec4 ang_mom = a->cart_to_wheel(ang_mom_xyz);

		for (int j = 0; j < 4; j++)
		{
			state[7 + j] = ang_mom(j);
		}

		trajectory_history.push_back(state);
		
		ix_float += increment;
		ix = std::floor(ix_float);
	}

	for (int j = 0; j < 7; j++)
	{
		state[j] = rk.trajectory_history.back()[j];
	}

	std::array<float, 3> ang_mom_xyz_array;
	for (int j = 0; j < 3; j++)
	{
		ang_mom_xyz_array[j] = rk.trajectory_history.back()[7 + j];
	}
	vec3 ang_mom_xyz = vec3(ang_mom_xyz_array[0], ang_mom_xyz_array[1], ang_mom_xyz_array[2]);
	vec4 ang_mom = a->cart_to_wheel(ang_mom_xyz);

	for (int j = 0; j < 4; j++)
	{
		state[7 + j] = ang_mom(j);
	}

	trajectory_history.push_back(state);

	return trajectory_history;
}

std::vector<dynamics::control_t> diagnostics::rk_control_to_vector_of_states(rk7<dynamics> rk, dynamics::control_t constant_control)
{
	int points = 500;
	if (points > rk.trajectory_history.size()) points = rk.trajectory_history.size();

	std::vector<dynamics::control_t> control_history;

	for (int i = 0; i < points - 1; i++)
	{
		control_history.push_back(constant_control);
	}

	control_history.push_back(constant_control);

	return control_history;
}

std::vector<float> diagnostics::rk_time_to_vector_of_floats(rk7<dynamics> rk)
{
	int points = 500;
	if (points > rk.trajectory_history.size()) points = rk.trajectory_history.size();

	float increment = float(rk.trajectory_history.size()) / float(points);

	float ix_float = 0.0;
	int ix = 0;

	std::vector<float> time_history;

	for (int i = 0; i < points - 1; i++)
	{
		time_history.push_back(rk.time_history[ix]);

		ix_float += increment;
		ix = std::floor(ix_float);
	}

	time_history.push_back(rk.time_history.back());

	return time_history;
}


void diagnostics::eigen_type_to_file(const Eigen::MatrixXf& matrix, std::string name)
{
	std::ofstream out_file;
	out_file.open(name, std::ios_base::trunc);
	out_file << std::fixed << std::setprecision(8) << std::endl;

	for (int a = 0; a < matrix.rows(); a++)
	{
		for (int b = 0; b < matrix.cols(); b++)
		{
			out_file << std::setw(16) << matrix(a, b) << "\t";
		}
		out_file << std::endl;
	}
}