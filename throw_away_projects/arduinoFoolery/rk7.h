#pragma once

#include <array>
#include <vector>
#include <fstream>
#include <iomanip>

namespace ADCS
{
    template <typename model_t>
    struct rk7
    {
        using state_t = typename model_t::state_t;

        std::vector<state_t> trajectory_history;
        std::vector<float> time_history;

        std::array<state_t, 11> derivatives;
        std::array<state_t, 10> states;

        bool gather_history = false;

        size_t state_length = std::tuple_size<state_t>::value;

        int to_file_skip_amount = 20;

        float current_time;

        model_t *model;

        rk7() = default;

        void set_model(model_t* input_model)
        {
            model = input_model;
        }

        void step(const float& time, state_t& state, const float& time_delta)
        {
            derivatives[0] = (*model).evaluate(time, state);

            for (size_t i=0; i < state_length; i++)
                states[0][i] = state[i] + 2.0 * time_delta / 27.0
                * derivatives[0][i];
            
            derivatives[1] = (*model).evaluate(time + 2.0 * time_delta / 27.0, states[0]);

            for (size_t i=0; i < state_length; i++)
                states[1][i] = state[i] + time_delta / 36.0
                * (derivatives[0][i] + 30. * derivatives[1][i]);

            derivatives[2] = (*model).evaluate(time + time_delta / 9.0, states[1]);

            for (size_t i=0; i < state_length; i++)
                states[2][i] = state[i] + time_delta / 24.0
                * (derivatives[0][i] + 3.0 * derivatives[2][i]);

            derivatives[3] = (*model).evaluate(time + time_delta / 6.0, states[2]);

            for (size_t i=0; i < state_length; i++)
                states[3][i] = state[i] + time_delta / 48.0
                * (20.0 * derivatives[0][i] - 75.0 * derivatives[2][i] + 75.0 * derivatives[3][i]);

            derivatives[4] = (*model).evaluate(time + 5.0 * time_delta / 12.0, states[3]);

            for (size_t i=0; i < state_length; i++)
                states[4][i] = state[i] + time_delta / 20.0
                * (derivatives[0][i] + 5.0 * derivatives[3][i] + 4.0 * derivatives[4][i]);

            derivatives[5] = (*model).evaluate(time + time_delta / 2.0, states[4]);

            for (size_t i=0; i < state_length; i++)
                states[5][i] = state[i] + time_delta / 108.0
                * (-25.0 * derivatives[0][i] + 125.0 * derivatives[3][i] - 260.0 * derivatives[4][i] + 250.0 * derivatives[5][i]);

            derivatives[6] = (*model).evaluate(time + 5.0 * time_delta / 6.0, states[5]);

            for (size_t i=0; i < state_length; i++)
                states[6][i] = state[i] + time_delta
                * (31.0 / 300.0 * derivatives[0][i] + 61.0 / 225.0 * derivatives[4][i] - 2.0 / 9.0 * derivatives[5][i] + 13.0 / 900.0 * derivatives[6][i]);

            derivatives[7] = (*model).evaluate(time + time_delta / 6.0, states[6]);

            for (size_t i=0; i < state_length; i++)
                states[7][i] = state[i] + time_delta
                * (2.0 * derivatives[0][i] - 53.0 / 6.0 * derivatives[3][i] + 704.0 / 45.0 * derivatives[4][i] - 107.0 / 9.0 * derivatives[5][i]
                + 67.0 / 90.0 * derivatives[6][i] + 3.0 * derivatives[7][i]);

            derivatives[8] = (*model).evaluate(time + 2.0 * time_delta / 3.0, states[7]);

            for (size_t i=0; i < state_length; i++)
                states[8][i] = state[i] + time_delta
                * (-91.0 / 108.0 * derivatives[0][i] + 23.0 / 108.0 * derivatives[3][i] - 976.0 / 135.0 * derivatives[4][i] + 311.0 / 54.0 * derivatives[5][i]
                - 19.0 / 60.0 * derivatives[6][i] + 17.0 / 6.0 * derivatives[7][i] - 1.0 / 12.0 * derivatives[8][i]);

            derivatives[9] = (*model).evaluate(time + time_delta / 3.0, states[8]);

            for (size_t i=0; i < state_length; i++)
                states[9][i] = state[i] + time_delta
                * (2383.0 / 4100.0 * derivatives[0][i] - 341.0 / 164.0 * derivatives[3][i] + 4496.0 / 1025.0 * derivatives[4][i] - 301.0 / 82.0 * derivatives[5][i]
                + 2133.0 / 4100.0 * derivatives[6][i] + 45.0 / 82.0 * derivatives[7][i] + 45.0 / 164.0 * derivatives[8][i] + 18.0 / 41.0 * derivatives[9][i]);

            derivatives[10] = (*model).evaluate(time + time_delta, states[9]);

            for (size_t i=0; i < state_length; i++)
                state[i] += time_delta *
                    (
                        41.0 / 840.0 * derivatives[0][i] +
                        34.0 / 105.0 * derivatives[5][i] +
                        9.0 / 35.0 * derivatives[6][i] +
                        9.0 / 35.0 * derivatives[7][i] + 
                        9.0 / 280.0 * derivatives[8][i] +
                        9.0 / 280.0 * derivatives[9][i] +
                        41.0 / 840.0 * derivatives[10][i]
                    );
        }

        state_t integrate(float begin_time, float end_time, state_t state, float time_delta)
        {
            if (gather_history == true)
            {
                size_t space_to_reserve = (end_time - begin_time) / time_delta + 2.0;
                trajectory_history.clear();
                trajectory_history.reserve(space_to_reserve);
                time_history.clear();
                time_history.reserve(space_to_reserve);

                current_time = begin_time;
                while (current_time < end_time)
                {
                    trajectory_history.push_back(state);
                    time_history.push_back(current_time);
                    step(current_time, state, time_delta);
                    current_time += time_delta;
                }

                // Rewind one step since so that we can take one last step to the end.
                current_time -= time_delta;

                time_delta = end_time - current_time;

                current_time += time_delta;

                step(current_time, state, time_delta);

                trajectory_history.push_back(state);
                trajectory_history.shrink_to_fit();
                time_history.push_back(current_time);
                time_history.shrink_to_fit();

                return state;
            }

            else
            {
                current_time = begin_time;
                while (current_time < end_time)
                {
                    step(current_time, state, time_delta);
                    current_time += time_delta;
                }
                // Rewind one step since so that we can take one last step to the end.
                current_time -= time_delta;

                time_delta = end_time - current_time;

                current_time += time_delta;

                step(current_time, state, time_delta);

                return state;
            }
        }

        void trajectory_to_file(std::string tag = "trajectory.out")
        {
            std::ofstream trajectory_file;
            trajectory_file.open(tag, std::ios_base::trunc);
            trajectory_file << std::fixed << std::setprecision(5) << std::endl;

            for (int a = 0; a < trajectory_history.size(); a = a + 1 + to_file_skip_amount)
            {
                for (float b : trajectory_history[a])
                {
                    trajectory_file << std::setw(16) << b << "\t";
                }
                trajectory_file << std::endl;
            }
            trajectory_file.close();
        }

        void time_to_file(std::string tag = "time.out")
        {
            std::ofstream time_file;
            time_file.open(tag, std::ios_base::trunc);
            time_file << std::fixed << std::setprecision(5) << std::endl;

            for (int a = 0; a < time_history.size(); a = a + 1 + to_file_skip_amount)
            {
                time_file << std::setw(16) << time_history[a];
                time_file << std::endl;
            }
            time_file.close();
        }

        void trajectory_to_plot_dir()
        {
            std::ofstream trajectory_file;
            trajectory_file.open("C:/Users/Boss/Documents/Repos/Python/Plot/trajectory.out", std::ios_base::trunc);
            trajectory_file << std::fixed << std::setprecision(5) << std::endl;

            for (int a = 0; a < trajectory_history.size(); a = a + 1 + to_file_skip_amount)
            {
                for (float b : trajectory_history[a])
                {
                    trajectory_file << std::setw(16) << b << "\t";
                }
                trajectory_file << std::endl;
            }
            trajectory_file.close();
        }
    };
}