#pragma once

#include "definitions.h"

namespace ADCS
{

	struct cart_state
	{

		// Data

		vec3 m_ang_pos = vec3::Zero();
		vec3 m_ang_vel = vec3::Zero();


		// Constructor
		cart_state() = default;
		cart_state(vec3 ang_pos, vec3 ang_vel);


		// Methods

		cart_state operator- (const cart_state& R);


	};
}
