#include "reaction_wheel.h"

using namespace ADCS;

void reaction_wheel::set_speed(const float& speed)
{
	motor.setspeed(speed);
}

void reaction_wheel::set_dir(const int& dir)
{
	motor.setrotation(dir);
}