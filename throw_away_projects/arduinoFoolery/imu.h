#pragma once

#include "definitions.h"

namespace ADCS
{
	struct imu
	{
		// Data

		std::string m_bbb_address;

		vec3 m_gravity_vec = vec3::Zero();
		vec3 m_ang_vel = vec3::Zero();
		//some_type rotation; // AR/VR Stabilized Game Rotation vector 

		// Constructor

		imu() = default;


		// Methods


	};
}