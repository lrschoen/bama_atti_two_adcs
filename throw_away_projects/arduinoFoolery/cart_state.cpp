#include "cart_state.h"

using namespace ADCS;

cart_state::cart_state(vec3 ang_pos, vec3 ang_vel)
{
	m_ang_pos = ang_pos;
	m_ang_vel = ang_vel;
}

cart_state cart_state::operator- (const cart_state& R)
{
	cart_state out;

	out.m_ang_pos = m_ang_pos - R.m_ang_pos;
	out.m_ang_vel = m_ang_vel - R.m_ang_vel;

	return out;
}