#pragma once

#include "definitions.h"

namespace ADCS
{

	struct quaternion
	{

		// Data
		
		vec3 m_vector = vec3::Zero();
		float m_scalar = 0.0f;


		// Constructor
		quaternion() = default;
		quaternion(const vec3& axis, const float& angle_degree);

		// Methods

		quaternion get_derivative(const vec3& angular_vel) const;
		void get_axis_angle_representation(vec3& axis, float& angle);
		static quaternion from_components(float i, float j, float k, float s);
		quaternion multiply(const quaternion&);
		void normalize();
		float get_norm();
		static quaternion get_error(const quaternion& current, const quaternion& desired);



	};
}