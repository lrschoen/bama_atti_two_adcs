#pragma once

#include <array>
#include <vector>
#include <fstream>
#include <iomanip>

#include "rk7.h"

namespace ADCS
{

    template <typename model_t>
    struct rk7_control : rk7<model_t>
    {
        using state_t = typename model_t::state_t;
        using control_t = typename model_t::control_t;

        std::vector<control_t> controls;
        std::vector<control_t> control_history;
        
        size_t control_length = std::tuple_size<control_t>::value;

        control_t spacing;

        unsigned int count = 0;
        
        rk7_control() = default;

        void load_control_history(const std::vector<control_t>& input_control_history)
        {
            control_history = input_control_history;
        }

        
        void generate_constant_control(float begin_time, float end_time, control_t control, float time_delta)
        {
            size_t new_size = (end_time - begin_time) / time_delta + 1;
            control_history.resize(new_size);
            std::fill(control_history.begin(), control_history.end(), control);
        }

        void generate_linear_control(std::vector<float> input_times, std::vector<control_t> input_controls, float time_delta)
        {
            const int num_points = input_times.size();
            int intervals = 0;
            int a = 0;

            size_t new_size = (input_times.back() - input_times.front()) / time_delta + 2;
            controls.resize(new_size);

            for (int i = 0; i < num_points - 1; i++)
            {
                intervals = std::floor((input_times[i + 1] - input_times[i]) / time_delta);

                for (int j = 0; j < control_length; j++)
                {
                    spacing[j] = (input_controls[i + 1][j] - input_controls[i][j]) / intervals;
                }

                for (int j = 0; j < intervals; j++)
                {
                    for (int k = 0; k < control_length; k++)
                    {
                        controls[a][k] = input_controls[i][k] + j * spacing[k];
                    }
                    a++;
                }
            }
            for (int k = 0; k < control_length; k++)
            {
                controls[new_size - 2][k] = input_controls[num_points - 1][k];
                controls[new_size - 1][k] = input_controls[num_points - 1][k];
            }
        }

        void step(const float& time, state_t& state, const control_t& control, const float& time_delta)
        {
            this->derivatives[0] = (*this->model).evaluate(time, state, control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[0][i] = state[i] + 2.0f * time_delta / 27.0f
                * this->derivatives[0][i];
            
            this->derivatives[1] = (*this->model).evaluate(time + 2.0f * time_delta / 27.0f, this->states[0], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[1][i] = state[i] + time_delta / 36.0f
                * (this->derivatives[0][i] + 30.0f * this->derivatives[1][i]);

            this->derivatives[2] = (*this->model).evaluate(time + time_delta / 9.0f, this->states[1], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[2][i] = state[i] + time_delta / 24.0
                * (this->derivatives[0][i] + 3.0f * this->derivatives[2][i]);

            this->derivatives[3] = (*this->model).evaluate(time + time_delta / 6.0, this->states[2], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[3][i] = state[i] + time_delta / 48.0f
                * (20.0f * this->derivatives[0][i] - 75.0f * this->derivatives[2][i] + 75.0f * this->derivatives[3][i]);

            this->derivatives[4] = (*this->model).evaluate(time + 5.0f * time_delta / 12.0f, this->states[3], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[4][i] = state[i] + time_delta / 20.0f
                * (this->derivatives[0][i] + 5.0f * this->derivatives[3][i] + 4.0f * this->derivatives[4][i]);

            this->derivatives[5] = (*this->model).evaluate(time + time_delta / 2.0f, this->states[4], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[5][i] = state[i] + time_delta / 108.0f
                * (-25.0f * this->derivatives[0][i] + 125.0f * this->derivatives[3][i] - 260.0f * this->derivatives[4][i] + 250.0f * this->derivatives[5][i]);

            this->derivatives[6] = (*this->model).evaluate(time + 5.0f * time_delta / 6.0f, this->states[5], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[6][i] = state[i] + time_delta
                * (31.0f / 300.0f * this->derivatives[0][i] + 61.0f / 225.0f * this->derivatives[4][i] - 2.0f / 9.0f * this->derivatives[5][i] + 13.0f / 900.0f * this->derivatives[6][i]);

            this->derivatives[7] = (*this->model).evaluate(time + time_delta / 6.0, this->states[6], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[7][i] = state[i] + time_delta
                * (2.0f * this->derivatives[0][i] - 53.0f / 6.0f * this->derivatives[3][i] + 704.0f / 45.0f * this->derivatives[4][i] - 107.0f / 9.0f * this->derivatives[5][i]
                + 67.0f / 90.0f * this->derivatives[6][i] + 3.0f * this->derivatives[7][i]);

            this->derivatives[8] = (*this->model).evaluate(time + 2.0f * time_delta / 3.0f, this->states[7], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[8][i] = state[i] + time_delta
                * (-91.0f / 108.0f * this->derivatives[0][i] + 23.0f / 108.0 * this->derivatives[3][i] - 976.0f / 135.0f * this->derivatives[4][i] + 311.0f / 54.0f * this->derivatives[5][i]
                - 19.0f / 60.0f * this->derivatives[6][i] + 17.0f / 6.0f * this->derivatives[7][i] - 1.0f / 12.0f * this->derivatives[8][i]);

            this->derivatives[9] = (*this->model).evaluate(time + time_delta / 3.0, this->states[8], control);

            for (size_t i=0; i < this->state_length; i++)
                this->states[9][i] = state[i] + time_delta
                * (2383.0 / 4100.0 * this->derivatives[0][i] - 341.0 / 164.0 * this->derivatives[3][i] + 4496.0 / 1025.0 * this->derivatives[4][i] - 301.0 / 82.0 * this->derivatives[5][i]
                + 2133.0 / 4100.0 * this->derivatives[6][i] + 45.0 / 82.0 * this->derivatives[7][i] + 45.0 / 164.0 * this->derivatives[8][i] + 18.0 / 41.0 * this->derivatives[9][i]);

            this->derivatives[10] = (*this->model).evaluate(time + time_delta, this->states[9], control);

            for (size_t i=0; i < this->state_length; i++)
                state[i] += time_delta *
                    (
                        41.0f / 840.0f * this->derivatives[0][i] +
                        34.0f / 105.0f * this->derivatives[5][i] +
                        9.0f / 35.0f * this->derivatives[6][i] +
                        9.0f / 35.0f * this->derivatives[7][i] + 
                        9.0f / 280.0f * this->derivatives[8][i] +
                        9.0f / 280.0f * this->derivatives[9][i] +
                        41.0f / 840.0f * this->derivatives[10][i]
                    );
        }

        state_t integrate(float begin_time, float end_time, state_t state, float time_delta)
        {
            if (this->gather_history == true)
            {
                size_t space_to_reserve = (end_time - begin_time) / time_delta + 2.0;
                this->trajectory_history.reserve(space_to_reserve);
                this->time_history.clear();
                this->time_history.reserve(space_to_reserve);

                count = 0;
                this->current_time = begin_time;
                while (this->current_time < end_time)
                {
                    this->trajectory_history.push_back(state);
                    this->time_history.push_back(this->current_time);
                    step(this->current_time, state, control_history[count], time_delta);
                    this->current_time += time_delta;
                    count++;
                }
                this->trajectory_history.push_back(state);

                // Rewind one step since so that we can take one last step to the end.
                this->current_time -= time_delta;

                time_delta = end_time - this->current_time;

                this->current_time += time_delta;

                step(this->current_time, state, control_history[count - 1], time_delta);

                this->trajectory_history.push_back(state);
                this->trajectory_history.shrink_to_fit();
                this->time_history.push_back(this->current_time);
                this->time_history.shrink_to_fit();

                return state;
            }

            else
            {
                count = 0;
                this->current_time = begin_time;
                while (this->current_time < end_time)
                {
                    step(this->current_time, state, control_history[count], time_delta);
                    this->current_time += time_delta;
                    count++;
                }
                // Rewind one step since so that we can take one last step to the end.
                this->current_time -= time_delta;

                time_delta = end_time - this->current_time;

                this->current_time += time_delta;

                step(this->current_time, state, control_history[count - 1], time_delta);

                return state;
            }
        }

        void control_to_file()
        {
            std::ofstream control_file;
            control_file.open("control.txt", std::ios_base::trunc);
            control_file << std::fixed << std::setprecision(5) << std::endl;

            for (int a = 0; a < control_history.size(); a = a + 1 + this->to_file_skip_amount)
            {
                for (float b : control_history[a])
                {
                    control_file << std::setw(16) << b << "\t";
                }
                control_file << std::endl;
            }
            control_file.close();
        }
    };
};