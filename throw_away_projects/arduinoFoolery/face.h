#pragma once

#include <array>

#include "css.h"

namespace ADCS
{
	struct face
	{
		// Data

		static constexpr int m_num_of_sensors_per_face = 4;

		std::array<css, m_num_of_sensors_per_face> css_list_m;
		
		vec3 m_normal_vec;

		// Constructor

		face() = default;

		// Methods

		void set_normal_vec(vec3 vec);


	};
}