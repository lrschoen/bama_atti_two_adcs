#include "dynamics.h"

using namespace ADCS;


dynamics::dynamics()
{
	// Initialize moment of inertia of the cubesat
	m_I(0, 0) = m_Ix;
	m_I(1, 1) = m_Iy;
	m_I(2, 2) = m_Iz;

	m_rk.set_model(this);
}


state_def dynamics::get_state_derivative(const state_def& state, const vec4& torque_controls)
{
	state_def state_dot;

	state_dot.m_q = state.m_q.get_derivative(state.m_ang_vel);
	state_dot.m_ang_vel = m_I.inverse() *
							(
								-state.m_ang_vel.cross(m_I * state.m_ang_vel)
								-state.m_ang_vel.cross(m_actuators.m_B * state.m_ang_mom_wheels)
								+ m_actuators.m_B * torque_controls
							);
	state_dot.m_ang_mom_wheels = -torque_controls;

	return state_dot;
}

state_def dynamics::step_current_state(const state_def& state, const vec4& torques)
{
	control_t constant_control = state_def::map_torques_to_array<control_t>(torques);
	m_rk.generate_constant_control(0.0, update_interval, constant_control, m_integration_dt);

	state_t array_current_state = state.map_to_array<state_t>();
	array_current_state = m_rk.integrate(0.0, update_interval, array_current_state, m_integration_dt);
	state_def stepped_state = state_def::map_from_array<state_t>(array_current_state);

	return stepped_state;
}

dynamics::state_t dynamics::evaluate(const float& /*time*/, const state_t& state)
{
	state_def derivative = get_state_derivative(state_def::map_from_array<state_t>(state), vec4::Zero());
	state_t array_derivative = derivative.map_to_array<state_t>();

	return array_derivative;
}

dynamics::state_t dynamics::evaluate(const float& /*time*/, const state_t& state, const control_t& control)
{
	state_def derivative = get_state_derivative(state_def::map_from_array<state_t>(state), vec4(control[0], control[1], control[2], control[3]));
	state_t array_derivative = derivative.map_to_array<state_t>();

	return array_derivative;
}

void dynamics::spin_up()
{
	m_actuators.do_spin_up();
	m_current_state.m_ang_mom_wheels = m_actuators.get_current_wheel_ang_mom();
}

state_def dynamics::get_current_posvel(state_def& current_state)
{
	// Assign pos and vel

	return current_state;
}