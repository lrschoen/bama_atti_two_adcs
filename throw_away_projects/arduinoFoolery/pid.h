#pragma once

#include "Core"

#include "dynamics.h"
#include "timing.h"


namespace ADCS
{
	struct pid : Timeable
	{
		using gain_t = Eigen::Matrix3f;

		using error_t = vec3;

		// Data

		float m_elapsed_time = 0.0f;
		float m_telemetry_timer = 0.0f;

		const vec3 m_detumble_gains = vec3(0.0f, 0.0f, 0.15f);
		const vec3 m_track_gains = vec3(0.05f, 0.00001f, 0.4f);
		const vec3 m_slew_gains = vec3(0.05f, 0.0f, 0.4f);

		gain_t m_p_gains = gain_t::Identity();
		gain_t m_i_gains = gain_t::Identity();
		gain_t m_d_gains = gain_t::Identity();

		dynamics m_dynamics;

		quaternion m_desired_state;

		error_t m_p_error = error_t::Zero();
		error_t m_i_error = error_t::Zero();
		error_t m_d_error = error_t::Zero();
		error_t m_p_error_cache = error_t::Zero();

		error_t m_output = error_t::Zero();

		std::vector<dynamics::state_t> m_attitude_history;
		std::vector<float> m_time_history;
		std::vector<dynamics::control_t> m_wheel_torque_history;

		// Constructor

		pid() = default;

		// Methods

		void set_gains(float p, float i, float d);
		void set_gains(vec3 gains);
		void set_gains_using_MOI(vec3 gains);

		void set_desired_state(quaternion desired_state);

		void compute_error(const state_def& state);
		void accumulate_integral_error(const error_t& e);
		void purge_error();
		void do_iteration();
		void sim_iteration();
		void rest();

		void print_error_stats();
		void capture_telemetry_data();


	};
}