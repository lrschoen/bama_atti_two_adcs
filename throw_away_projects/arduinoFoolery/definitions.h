#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <array>
#include <vector>
#include "Dense"


using vec3 = Eigen::Vector3f;
using vec4 = Eigen::Vector4f;

static constexpr float update_interval = 1e-1f; // Assumed interval between control loop iterations, used for simulating pid iterations
static constexpr float telemetry_capture_interval = 0.3f; // Capture telemetry data at this interval

static constexpr float PI = 3.14159265358979323846264f;

static const vec3 x_axis = vec3(1.0f, 0.0f, 0.0f);
static const vec3 y_axis = vec3(0.0f, 1.0f, 0.0f);
static const vec3 z_axis = vec3(0.0f, 0.0f, 1.0f);

static float to_rad(float angle)
{
	angle *= PI / 180.0f;
	return angle;
}

static float to_deg(float angle)
{
	angle *= 180.0f / PI;
	return angle;
}