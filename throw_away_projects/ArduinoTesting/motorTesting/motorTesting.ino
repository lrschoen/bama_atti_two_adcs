#include <PWM.h>

int pwm_pin = 3;
int CW_pin = 2;
int CCW_pin = 1;

double maxpwm = 255.0;
double percent = 90.0;
double pwmval = percent / 100.0 * maxpwm;
double freq = 53.6e3;

void setup()
{
  
  pinMode(pwm_pin, OUTPUT);
  pinMode(CW_pin, OUTPUT);
  pinMode(CCW_pin, OUTPUT);
  
  //SetPinFrequencySafe(pwm_pin, freq);
  
  digitalWrite(CW_pin, HIGH);
  digitalWrite(CCW_pin, LOW);
  analogWrite(pwm_pin, pwmval);
  
  //delay(30.0*1e3);
  
  //analogWrite(pwm_pin, 0.0);
  //digitalWrite(CW_pin, LOW);
  //digitalWrite(CCW_pin, LOW);

}

void loop()
{
  //exit(0);
}
