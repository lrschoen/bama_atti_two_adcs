#pragma once
#include "PWM.h"
#include "GPIO.h"
using namespace exploringBB;
class MotorController
{
public:
	PWM * pwm;
	GPIO * gpioCW;
	GPIO * gpioCCW;
	double period = 200000;
	MotorController(string, int pwmName, int pwmNum, int gpioNum1, int gpioNum2);
	void setrotation(int);
	void setspeed(double);
	void init();
	void run();
	void stop();
	void cleanup();
	double getSpeed();
};

