#include <cstdio>
#include<iostream>
#include <sstream>
#include<iomanip>
#include "GPIO.h"
#include "PWM.h"
#include "MotorController.h"
#include <thread>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include "Timing.h"
#include "cstring"
#include <chrono>
#include <fstream>
#include <vector>

using namespace exploringBB;

std::vector<MotorController*> motors;

void setPWM(float ns) {
	motors[0]->setspeed(ns);
	motors[1]->setspeed(ns);
	motors[0]->run();
	motors[1]->run();
}
void setPWM(float ns, float ns1) {
	motors[0]->setspeed(ns);
	motors[1]->setspeed(ns1);
	motors[0]->run();
	motors[1]->run();
}

void StopMotors() {
	motors[0]->stop();
	motors[0]->cleanup();
}

int main()
{
	float PWM0 = 50.0;


	motors.push_back(new MotorController("P9_16", 4, 1, 44, 26));

	motors[0]->init();
	std::this_thread::sleep_for(std::chrono::milliseconds(200));

	motors[0]->setspeed(PWM0);
	motors[0]->setrotation(1);
	motors[0]->run();

	StopMotors();

	return 0;
}