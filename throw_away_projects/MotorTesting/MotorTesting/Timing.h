#pragma once
#include <thread>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::_V2::system_clock::time_point TimePoint;
struct TimeController {
	

	TimePoint tInit;
	TimePoint t1;
	TimePoint t2;

	void startclock() {
		t1 = Clock::now();
	}
	float getdt_restart_ms() {
		t2 = Clock::now();
		float dt = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
		t1 = Clock::now();
		return dt;
	}
	float getdt_restart_micros() {
		t2 = Clock::now();
		float dt = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
		t1 = Clock::now();
		return dt;
	}
	float getdt_restart_nans() {
		t2 = Clock::now();
		float dt = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
		t1 = Clock::now();
		return dt;
	}
	float getdt_ms() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::milliseconds>(tc - t1).count();
		return dt;
	}
	float getdt_micros() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::microseconds>(tc - t1).count();
		return dt;
	}
	float getdt_nans() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::nanoseconds>(tc - t1).count();
		return dt;
	}
	double gettotal_ms() {
		TimePoint tc = Clock::now();;
		double dt = std::chrono::duration_cast<std::chrono::milliseconds>(tc - tInit).count();
		return dt;
	}
	double gettotal_ns() {
		TimePoint tc = Clock::now();;
		double dt = std::chrono::duration_cast<std::chrono::nanoseconds>(tc - tInit).count();
		return dt;
	}
	double gettotal_s() {
		TimePoint tc = Clock::now();;
		double dt = std::chrono::duration_cast<std::chrono::seconds>(tc - tInit).count();
		return dt;
	}
	template<class T>
	void sleepfor_s(T s) {
		int si = int(s);
		std::this_thread::sleep_for(std::chrono::seconds(si));
	}
	template<class T>
	void sleepfor_ms(T ms) {
		int msi = int(ms);
		std::this_thread::sleep_for(std::chrono::milliseconds(msi));
	}
	template<class T>
	void sleepfor_nans(T nans) {
		int nansi = int(nans);
		std::this_thread::sleep_for(std::chrono::nanoseconds(nansi));
	}
	TimeController() {
		tInit = Clock::now();
	}
};

struct Timeable {
	TimeController * DT;
	///////////////////////////////////////////
	double Stamp_Delta_ms;
	double Stamp_P_ms;
	double Stamp_N_ms;
	double avg_Delta_ms =0;
	double count = 0;

	////////////////////////////////////////////
	double inner_Stamp_ms;
	double inner_avg_ms =0;
	double inner_count =0;
	////////////////////////////////////////////

	
	void Init() {
		Stamp_P_ms = DT->gettotal_ns()/1000.0;
		Stamp_N_ms = DT->gettotal_ns()/1000.0;
		Stamp_Delta_ms = Stamp_N_ms - Stamp_P_ms;
	}
	void Stamp() {
		Stamp_P_ms = Stamp_N_ms;
		Stamp_N_ms = DT->gettotal_ns()/1000.0;
		Stamp_Delta_ms = Stamp_N_ms - Stamp_P_ms;
		avg_Delta_ms = (Stamp_Delta_ms + count * avg_Delta_ms)/(count+1);
		count++;
	}
	float Delta_float() { return float(this->Stamp_Delta_ms); }
	double Delta_double() { return (this->Stamp_Delta_ms); }

	float avg_Delta_float() { return float(this->Stamp_Delta_ms); }
	double avg_Delta_double() { return (this->Stamp_Delta_ms); }

	void start_inner_timer() {
		inner_Stamp_ms = DT->gettotal_ns() / 1000.0;
	}
	double stop_inner_timer() {
		double dt = DT->gettotal_ns() / 1000.0 - inner_Stamp_ms;
		inner_avg_ms = (dt + inner_count* inner_avg_ms) / (inner_count + 1);
		inner_count++;
		return dt;
	}
	Timeable(TimeController * dt) {
		this-> DT = dt;
	}
	Timeable() {

	}
	void SetTimeController(TimeController * dt) {
		this->DT = dt;
		this->Init();
	}
};