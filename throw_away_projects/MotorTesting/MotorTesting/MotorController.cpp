#include "MotorController.h"



MotorController::MotorController(string pinName, int pwmName, int pwmNum, int gpioNumCW, int gpioNumCCW)
{
	this->gpioCW = new GPIO(gpioNumCW);
	this->gpioCCW = new GPIO(gpioNumCCW);
	this->pwm = new PWM(pinName, pwmName, pwmNum);
}
void MotorController::setrotation(int value)
{
	//a 1 is CW and a 0 is CCW
	if (value == 1)
	{
		this->gpioCCW->setValue(GPIO::LOW);
		this->gpioCW->setValue(GPIO::HIGH);
	}
	else if (value == 0)
	{
		this->gpioCW->setValue(GPIO::LOW);
		this->gpioCCW->setValue(GPIO::HIGH);
	}
	else
	{
		this->gpioCW->setValue(GPIO::LOW);
		this->gpioCCW->setValue(GPIO::LOW);
	}
}
void MotorController::setspeed(double percentage) 
{
	this->pwm->setDutyCycle(float(percentage));
}
void MotorController::init()
{
	this->pwm->initialize();
	this->pwm->setPeriod(this->period);
	this->pwm->setPolarity("normal");
	this->gpioCW->setDirection(GPIO::OUTPUT);
	this->gpioCCW->setDirection(GPIO::OUTPUT);
}
void MotorController::run()
{
	this->pwm->run();
}
void MotorController::stop()
{
	this->pwm->stop();
}
void MotorController::cleanup()
{
	this->pwm->uninitialize();
	this->gpioCW->unexportGPIO();
	this->gpioCCW->unexportGPIO();
}
