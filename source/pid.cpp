#include "pid.h"

using namespace ADCS;

void pid::set_gains(float p, float i, float d)
{
	m_p_gains *= p;
	m_i_gains *= i;
	m_d_gains *= d;
}

void pid::set_gains(vec3 gains)
{
	m_p_gains *= gains(0);
	m_i_gains *= gains(1);
	m_d_gains *= gains(2);
}

void pid::set_gains_using_MOI(vec3 gains)
{
	m_p_gains = m_dynamics.m_I * gains(0);
	m_i_gains = m_dynamics.m_I * gains(1);
	m_d_gains = m_dynamics.m_I * gains(2);
}

void pid::set_desired_state(quaternion q_desired)
{
	m_desired_state = q_desired;
}

void pid::do_iteration()
{
	start_inner_timer();
	m_dynamics.get_current_posvel(m_dynamics.m_current_state);
	m_dynamics.m_current_state.m_ang_mom_wheels = m_dynamics.m_actuators.apply_torques(m_dynamics.m_current_torques, get_avg_inner_timer());
	
	if (m_telemetry_timer >= telemetry_capture_interval) // Capture telemetry every 0.3 seconds
	{
		capture_telemetry_data();
		m_telemetry_timer = 0.0;
	}

	compute_error(m_dynamics.m_current_state);
	m_output = m_p_gains * m_p_error + m_i_gains * m_i_error + m_d_gains * m_d_error;
	m_dynamics.m_current_torques = m_dynamics.m_actuators.cart_to_wheel(m_output);
	

	m_elapsed_time = get_time_since_init();
	m_telemetry_timer += stop_inner_timer();
}

void pid::sim_iteration() // Integrates state instead of using sensors to update state, and sends no output to actuators
{
	capture_telemetry_data();

	m_dynamics.m_current_state = m_dynamics.step_current_state(m_dynamics.m_current_state, m_dynamics.m_current_torques);

	compute_error(m_dynamics.m_current_state);
	m_output = m_p_gains * m_p_error + m_i_gains * m_i_error + m_d_gains * m_d_error;
	m_dynamics.m_current_torques = m_dynamics.m_actuators.cart_to_wheel(m_output);

	m_elapsed_time += update_interval;
}

void pid::rest()
{
	start_inner_timer();
	m_dynamics.get_current_posvel(m_dynamics.m_current_state);

	if (m_telemetry_timer >= telemetry_capture_interval) // Capture telemetry every 0.3 seconds
	{
		capture_telemetry_data();
		m_telemetry_timer = 0.0;
	}

	m_elapsed_time = get_time_since_init();
	m_telemetry_timer += stop_inner_timer_no_avg();
}

void pid::compute_error(const state_def& state)
{
	quaternion q_error = quaternion::get_error(state.m_q, m_desired_state);
	pid::error_t e = -q_error.m_vector;
	m_p_error = std::round(q_error.m_scalar / std::abs(q_error.m_scalar)) * e;
	accumulate_integral_error(e);
	m_d_error = -state.m_ang_vel;
}

void pid::accumulate_integral_error(const pid::error_t& e)
{
	m_i_error += (e + m_p_error_cache) / 2.0 * update_interval;
	m_p_error_cache = e;
}

void pid::purge_error()
{
	m_p_error_cache = error_t::Zero();
	m_p_error = error_t::Zero();
	m_i_error = error_t::Zero();
	m_d_error = error_t::Zero();
}

void pid::print_error_stats()
{
	std::cout << std::setw(16) << "m_output"  << "\t" << std::setw(16) << "m_p_error"  << "\t" << std::setw(16) << "m_i_error"  << "\t" << std::setw(16) << "m_d_error"  << std::endl;
	std::cout << std::setw(16) << m_output(0) << "\t" << std::setw(16) << m_p_error(0) << "\t" << std::setw(16) << m_i_error(0) << "\t" << std::setw(16) << m_d_error(0) << std::endl;
	std::cout << std::setw(16) << m_output(1) << "\t" << std::setw(16) << m_p_error(1) << "\t" << std::setw(16) << m_i_error(1) << "\t" << std::setw(16) << m_d_error(1) << std::endl;
	std::cout << std::setw(16) << m_output(2) << "\t" << std::setw(16) << m_p_error(2) << "\t" << std::setw(16) << m_i_error(2) << "\t" << std::setw(16) << m_d_error(2) << std::endl;
	std::cout << "\n\n";
}

void pid::capture_telemetry_data()
{
	m_attitude_history.push_back(m_dynamics.m_current_state.map_to_array<dynamics::state_t>());

	m_time_history.push_back(m_elapsed_time);
	m_wheel_torque_history.push_back(state_def::map_torques_to_array<dynamics::control_t>(m_dynamics.m_current_torques));
}