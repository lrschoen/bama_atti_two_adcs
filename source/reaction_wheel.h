#pragma once

#include "definitions.h"
#include "MotorControllerDummy.h"

namespace ADCS
{
	struct reaction_wheel
	{
		// Data
		
		vec3 m_normal_vector;
		MotorController motor;


		// Constructor
		reaction_wheel() = default;

		// Methods
		
		void set_speed(const float& speed);

		void set_dir(const int& dir);

	};
}