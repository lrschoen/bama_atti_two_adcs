#pragma once

#include <array>

#include "reaction_wheel.h"

namespace ADCS
{
	struct actuators
	{
		// Data
		static constexpr int m_num_of_reaction_wheels = 4;

		static constexpr float m_wheel_MOI = 1.928886334e-4f; // kg*m^2   CHECK THIS
		static constexpr float m_motor_max_rpm = 4530.0f; // rotations per minute
		static constexpr float m_motor_max_ang_vel = m_motor_max_rpm * 2.0f * PI / 60.0f ; // rad/s   

		std::array<vec3, m_num_of_reaction_wheels> m_normal_vectors;
		std::array<reaction_wheel, m_num_of_reaction_wheels> m_reaction_wheel_list;

		std::array<std::string, m_num_of_reaction_wheels> m_pwm_names = std::array<std::string, m_num_of_reaction_wheels>{"P", "P", "P", "P"};
		std::array<int, m_num_of_reaction_wheels> m_pwm_pin_names = std::array<int, m_num_of_reaction_wheels>{0, 0, 0, 0};
		std::array<int, m_num_of_reaction_wheels> m_pwm_pin_numbers = std::array<int, m_num_of_reaction_wheels>{0, 0, 0, 0};
		std::array<int, m_num_of_reaction_wheels> m_CW_pin_numbers = std::array<int, m_num_of_reaction_wheels>{0, 0, 0, 0};
		std::array<int, m_num_of_reaction_wheels> m_CCW_pin_numbers = std::array<int, m_num_of_reaction_wheels>{0, 0, 0, 0};

		Eigen::Matrix<float, 3, 4> m_B = Eigen::Matrix<float, 3, 4>::Zero(); // Torque basis matrix (wheel to body)
		Eigen::Matrix<float, 4, 3> m_W = Eigen::Matrix<float, 4, 3>::Zero(); // Torque basis matrix (min. norm body to wheel)

		float m_orthogonal_wheel_spin_up_pwm_percent = 0.4f;
		float m_angled_wheel_spin_up_pwm_percent = std::sqrt(3.0f * m_orthogonal_wheel_spin_up_pwm_percent * m_orthogonal_wheel_spin_up_pwm_percent);

		vec4 m_spin_up_pwm_percents = vec4(
			m_orthogonal_wheel_spin_up_pwm_percent,
			m_orthogonal_wheel_spin_up_pwm_percent,
			m_orthogonal_wheel_spin_up_pwm_percent,
			m_angled_wheel_spin_up_pwm_percent
		);

		vec4 m_current_wheel_pwm_percents = vec4::Zero();

		Eigen::Vector4i m_spin_up_dirs = Eigen::Vector4i(0, 0, 0, 0);

		// Constructor
		actuators();

		// Methods
		
		void set_wheel_speeds_and_dirs(const vec4& pwm_percents, const Eigen::Vector4i& dirs);
		void set_wheel_speeds(const vec4& speeds);
		vec4 apply_torques(const vec4& wheel_torques, float dt); // returns new wheel ang vels
		vec4 cart_to_wheel(const vec3& cart);
		void do_spin_up();
		vec4 get_current_wheel_ang_mom();
		void stop_motors();



	};
}