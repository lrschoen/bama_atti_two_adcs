#include "css_array.h"

using namespace ADCS;

css_array::css_array()
{
	m_normal_vectors[0] = x_axis;
	m_normal_vectors[1] = y_axis;
	m_normal_vectors[2] = z_axis;
	m_normal_vectors[3] = -x_axis;
	m_normal_vectors[4] = -y_axis;
	m_normal_vectors[5] = -z_axis;

	for (int i = 0; i < m_normal_vectors.size(); i++)
	{
		m_face_list[i].set_normal_vec(m_normal_vectors[i]);
	}
}

vec3 css_array::get_sun_vec()
{
	vec3 s = m_H.transpose() * (m_H * m_H.transpose()).inverse() * m_y;

	return s;
}