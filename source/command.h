#pragma once

#include "pid.h"
#include "diagnostics.h"

namespace ADCS
{
	struct command
	{

		// Data

		pid m_pid;

		// Constructor
		command() = default;

		// Methods

		void initialize();
		void detumble(float time, bool purge_error=true); // Pure d controller
		void track(quaternion q_desired, float time, bool purge_error = true); // Full pid controller
		void slew(quaternion q_desired, float time, bool purge_error = true); // Pure pd controller
		void rest(float time); // No controller


		void do_control(float time);
		void shut_down();

		void attitude_history_to_file(std::string prefix = "BamaSat");

		void do_health_checks();
		

	};
}

