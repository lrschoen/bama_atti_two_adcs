#pragma once

#include "definitions.h"

namespace ADCS
{
	struct css
	{
		// Data

		float m_displacement_angle = 20.0f; // degrees
		float b = std::cos(to_rad(m_displacement_angle));
		float s = std::sqrt((1 - b*b) / 2.0f);
		float m_lux;

		bool los = false;

		std::string m_bbb_address;

		vec3 m_body_vec;

		// Constructor

		css() = default;

		// Methods

		float get_lux_from_sensor();

	};
}