#include "command.h"


using namespace ADCS;

void command::initialize()
{
	m_pid.m_dynamics.spin_up();
	m_pid.m_timer.initialize();

}

void command::detumble(float time, bool purge_error)
{
	if (purge_error == true)
	{
		m_pid.purge_error();
	}

	m_pid.set_gains_using_MOI(m_pid.m_detumble_gains);
	m_pid.set_desired_state(m_pid.m_dynamics.ref_q);

	do_control(time);
}

void command::track(quaternion q_desired, float time, bool purge_error)
{
	if (purge_error == true)
	{
		m_pid.purge_error();
	}
	
	m_pid.set_gains_using_MOI(m_pid.m_track_gains);
	m_pid.set_desired_state(q_desired);

	do_control(time);
}


void command::slew(quaternion q_desired, float time, bool purge_error)
{
	if (purge_error == true)
	{
		m_pid.purge_error();
	}

	m_pid.set_gains_using_MOI(m_pid.m_slew_gains);
	m_pid.set_desired_state(q_desired);

	do_control(time);
}

void command::rest(float time)
{
	float end_time = m_pid.m_elapsed_time + time;
	while (m_pid.m_elapsed_time < end_time)
	{
		m_pid.rest();
	}
}

void command::do_control(float time)
{
	float end_time = m_pid.m_elapsed_time + time;
	while (m_pid.m_elapsed_time < end_time)
	{
		m_pid.do_iteration();
	}

	std::cout << "avg loop:" << std::endl;
	std::cout << m_pid.get_avg_inner_timer() << "\n\n";
}

void command::shut_down()
{
	m_pid.m_dynamics.m_actuators.stop_motors();

}

void command::attitude_history_to_file(std::string prefix)
{
	std::string attitude_history_tag = prefix + "_attitude_history.txt";
	std::string time_tag = prefix + "_time.txt";
	std::string wheel_torque_history_tag = prefix + "_wheel_torque_history.txt";

	diagnostics::vector_of_states_to_file<dynamics::state_t>(m_pid.m_attitude_history, attitude_history_tag);
	diagnostics::vector_of_floats_to_file(m_pid.m_time_history, time_tag);
	diagnostics::vector_of_states_to_file<dynamics::control_t>(m_pid.m_wheel_torque_history, wheel_torque_history_tag);
}

void command::do_health_checks()
{
	bool green_light = true;
	std::vector<bool> health_flags;

	//health_flags.push_back(diagnostics::run_eom_check() );
	health_flags.push_back(diagnostics::run_pid_check() );

	for (auto x : health_flags)
	{
		if (x == false)
		{
			green_light = false;
		}
	}

	if (green_light == true)
	{
		std::cout << "Health checks good.\n\n";
	}
	else
	{
		std::cout << "One or more health check failed. Exiting now.\n\n";
		exit(1);
	}
}

