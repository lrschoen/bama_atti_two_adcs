#include "face.h"

using namespace ADCS;

void face::set_normal_vec(vec3 vec)
{
	int pointing_ix = 0;
	int sub_ix_1 = 0;
	int sub_ix_2 = 0;
	int sign = 0;

	float b = css_list_m[0].b;
	float s = css_list_m[0].s;

	m_normal_vec = vec;

	for (int i = 0; i < vec.size(); i++)
	{
		if (std::abs(vec(i)) > 0.9)
		{
			pointing_ix = i;
			if (vec(i) > 0)
			{
				sign = 1;
			}
			else
			{
				sign = -1;
			}
		}
	}

	if (pointing_ix == 0)
	{
		sub_ix_1 = 1;
		sub_ix_2 = 2;
	}
	else if (pointing_ix == 1)
	{
		sub_ix_1 = 0;
		sub_ix_2 = 2;
	}
	else if (pointing_ix == 2)
	{
		sub_ix_1 = 0;
		sub_ix_2 = 1;
	}
		

	for (int i = 0; i < vec.size(); i++)
	{
		css_list_m[i].m_body_vec(pointing_ix) = sign * b;
	}

	css_list_m[0].m_body_vec(sub_ix_1) = sign * s;
	css_list_m[0].m_body_vec(sub_ix_2) = sign * s;

	css_list_m[1].m_body_vec(sub_ix_1) = sign * s;
	css_list_m[1].m_body_vec(sub_ix_2) = -sign * s;

	css_list_m[2].m_body_vec(sub_ix_1) = -sign * s;
	css_list_m[2].m_body_vec(sub_ix_2) = sign * s;

	css_list_m[3].m_body_vec(sub_ix_1) = -sign * s;
	css_list_m[3].m_body_vec(sub_ix_2) = -sign * s;
}