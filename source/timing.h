#pragma once
#include <thread>
#include <chrono>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::steady_clock::time_point TimePoint;

struct TimeController {
	

	TimePoint m_tInit;
	TimePoint m_t1;
	TimePoint m_t2;

	TimeController() = default;

	void startclock() {
		m_t1 = Clock::now();
	}
	float getdt_restart_ms() {
		m_t2 = Clock::now();
		float dt = std::chrono::duration_cast<std::chrono::milliseconds>(m_t2 - m_t1).count();
		m_t1 = Clock::now();
		return dt;
	}
	float getdt_restart_micros() {
		m_t2 = Clock::now();
		float dt = std::chrono::duration_cast<std::chrono::microseconds>(m_t2 - m_t1).count();
		m_t1 = Clock::now();
		return dt;
	}
	float getdt_restart_nans() {
		m_t2 = Clock::now();
		float dt = std::chrono::duration_cast<std::chrono::nanoseconds>(m_t2 - m_t1).count();
		m_t1 = Clock::now();
		return dt;
	}
	float getdt_ms() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::milliseconds>(tc - m_t1).count();
		return dt;
	}
	float getdt_micros() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::microseconds>(tc - m_t1).count();
		return dt;
	}
	float getdt_nans() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::nanoseconds>(tc - m_t1).count();
		return dt;
	}
	float gettotal_ms() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::milliseconds>(tc - m_tInit).count();
		return dt;
	}
	float gettotal_ns() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::nanoseconds>(tc - m_tInit).count();
		return dt;
	}
	float gettotal_s() {
		TimePoint tc = Clock::now();;
		float dt = std::chrono::duration_cast<std::chrono::seconds>(tc - m_tInit).count();
		return dt;
	}
	template<class T>
	void sleepfor_s(T s) {
		int si = int(s);
		std::this_thread::sleep_for(std::chrono::seconds(si));
	}
	template<class T>
	void sleepfor_ms(T ms) {
		int msi = int(ms);
		std::this_thread::sleep_for(std::chrono::milliseconds(msi));
	}
	template<class T>
	void sleepfor_nans(T nans) {
		int nansi = int(nans);
		std::this_thread::sleep_for(std::chrono::nanoseconds(nansi));
	}
	void initialize()
	{
		m_tInit = Clock::now();
	}
	
};

struct Timeable {
	TimeController m_timer;

	Timeable() = default;

	float get_time_since_init()
	{
		return m_timer.gettotal_ms() / 1000.0f; // seconds
	}

	void start_inner_timer()
	{
		inner_Stamp_ms = m_timer.gettotal_ns() / 1000.0f;
	}
	
	float stop_inner_timer()
	{
		float dt_ms = m_timer.gettotal_ns() / 1000.0f - inner_Stamp_ms;
		inner_avg_ms = (dt_ms + inner_count * inner_avg_ms) / (inner_count + 1);
		inner_count++;

		return dt_ms / 1000.0f; // seconds
	}

	float stop_inner_timer_no_avg()
	{
		float dt_ms = m_timer.gettotal_ns() / 1000.0f - inner_Stamp_ms;
		return dt_ms / 1000.0f; // seconds
	}

	float get_avg_inner_timer()
	{
		return inner_avg_ms / 1000.0f; // seconds
	}

private:

	///////////////////////////////////////////
	float Stamp_Delta_ms;
	float Stamp_P_ms;
	float Stamp_N_ms;
	float avg_Delta_ms =0.0f;
	float count = 0.0f;

	////////////////////////////////////////////
	float inner_Stamp_ms;
	float inner_avg_ms =0.0f;
	float inner_count =0.0f;
	////////////////////////////////////////////

	
	void Init() {
		Stamp_P_ms = m_timer.gettotal_ns() / 1000.0f;
		Stamp_N_ms = m_timer.gettotal_ns() / 1000.0f;
		Stamp_Delta_ms = Stamp_N_ms - Stamp_P_ms;
	}
	void Stamp() {
		Stamp_P_ms = Stamp_N_ms;
		Stamp_N_ms = m_timer.gettotal_ns()/1000.0f;
		Stamp_Delta_ms = Stamp_N_ms - Stamp_P_ms;
		avg_Delta_ms = (Stamp_Delta_ms + count * avg_Delta_ms)/(count+1);
		count++;
	}
	
	
	float Delta_float() { return (float(this->Stamp_Delta_ms)); }

	float avg_Delta_float() { return float(this->Stamp_Delta_ms); }

	
	

	void SetTimeController(TimeController dt) {
		this->m_timer = dt;
		this->Init();
	}
};