#pragma once

#include "pid.h"


namespace ADCS
{

	struct diagnostics
	{
		// Data

		// Constructors

		diagnostics() = default;

		// Members

		static bool run_eom_check();
		static bool run_pid_check();

		static std::vector<dynamics::state_t> rk_history_to_vector_of_states(rk7<dynamics> rk, actuators* a);
		static std::vector<dynamics::control_t> rk_control_to_vector_of_states(rk7<dynamics> rk, dynamics::control_t constant_control);
		static std::vector<float> rk_time_to_vector_of_floats(rk7<dynamics> rk);

		void eigen_type_to_file(const Eigen::MatrixXf& matrix, std::string name);





		template <typename state_t>
		static void vector_of_states_to_file(std::vector<state_t> vec, std::string tag)
		{
			int points = 500;
			if (points > vec.size())
			{
				points = vec.size();
			}

			float increment = float(vec.size()) / float(points);

			std::ofstream file;
			file.open(tag, std::ios_base::trunc);
			file << std::fixed << std::setprecision(5) << std::endl;

			float ix_float = 0.0f;
			int ix = 0;

			for (int i = 0; i < points - 1; i++)
			{
				for (int j = 0; j < vec[i].size(); j++)
				{
					file << std::setw(16) << vec[ix][j] << "\t";
				}
				file << std::endl;

				ix_float += increment;
				ix = std::floor(ix_float);
			}

			for (int j = 0; j < vec.back().size(); j++)
			{
				file << std::setw(16) << vec.back()[j] << "\t";
			}
			file << std::endl;

			file.close();

		}

		static void vector_of_floats_to_file(std::vector<float> vec, std::string tag)
		{
			int points = 500;
			if (points > vec.size())
			{
				points = vec.size();
			}

			float increment = float(vec.size()) / float(points);

			std::ofstream file;
			file.open(tag, std::ios_base::trunc);
			file << std::fixed << std::setprecision(5) << std::endl;

			float ix_float = 0.0f;
			int ix = 0;

			for (int i = 0; i < points - 1; i++)
			{
				file << std::setw(16) << vec[ix] << "\t";
				file << std::endl;

				ix_float += increment;
				ix = std::floor(ix_float);
			}

			file << std::setw(16) << vec.back();
			file << std::endl;

			file.close();
		}
	};
}
