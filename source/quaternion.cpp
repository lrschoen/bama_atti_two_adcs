#include "quaternion.h"

using namespace ADCS;


quaternion::quaternion(const vec3& axis, const float& angle_degree)
{
	float angle_radian = to_rad(angle_degree);
	m_scalar = std::cos(angle_radian / 2.0f);
	m_vector = std::sin(angle_radian / 2.0f) * axis / axis.norm();
}

quaternion quaternion::get_derivative(const vec3& ang_vel) const
{
	quaternion q_dot;
	q_dot.m_vector = -0.5f * (ang_vel.cross(m_vector) - m_scalar * ang_vel);
	q_dot.m_scalar = -0.5f * (ang_vel.dot(m_vector));

	return q_dot;
}

void quaternion::get_axis_angle_representation(vec3& axis, float& angle)
{
	angle = -2.0f * std::acos(m_scalar);
	axis = m_vector / (std::sin(angle / 2.0f) );
}

quaternion quaternion::from_components(float i, float j, float k, float s)
{
	quaternion q;
	q.m_vector[0] = i;
	q.m_vector[1] = j;
	q.m_vector[2] = k;
	q.m_scalar = s;

	return q;
}

quaternion quaternion::multiply(const quaternion& q)
{
	quaternion product;
	product.m_scalar = m_scalar * q.m_scalar - m_vector.dot(q.m_vector);
	product.m_vector = m_vector.cross(q.m_vector) + m_scalar * q.m_vector + q.m_scalar * m_vector;

	return product;
}

void quaternion::normalize()
{
	float norm = get_norm();

	m_scalar /= norm;
	m_vector /= norm;
}

float quaternion::get_norm()
{
	float norm = std::sqrt(m_vector(0) * m_vector(0) + m_vector(1) * m_vector(1) + m_vector(2) * m_vector(2) + m_scalar * m_scalar);

	return norm;
}

quaternion quaternion::get_error(const quaternion& current, const quaternion& desired)
{
	quaternion e;
	e.m_scalar = current.m_scalar * desired.m_scalar + current.m_vector.dot(desired.m_vector);
	e.m_vector = -current.m_scalar * desired.m_vector + desired.m_scalar * current.m_vector - current.m_vector.cross(desired.m_vector);

	return e;
}
