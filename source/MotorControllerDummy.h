#pragma once
#include "PWMDummy.h"
#include "GPIODummy.h"

using namespace ADCS;

class MotorController
{
public:
	PWM * pwm;
	GPIO * gpioCW;
	GPIO * gpioCCW;
	float period = 200000.0f;
	MotorController() = default;
	void setup(string, int pwmName, int pwmNum, int gpioNum1, int gpioNum2);
	void setrotation(int);
	void setspeed(float);
	void init();
	void run();
	void stop();
	void cleanup();
};

