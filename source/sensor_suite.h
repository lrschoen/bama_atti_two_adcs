#pragma once

#include "css_array.h"
#include "imu_array.h"


namespace ADCS
{
	struct sensor_suite
	{
		// Data

		css_array m_css_array;
		imu_array m_imu_array;

		// Constructor

		sensor_suite() = default;

		// Methods


	};
}