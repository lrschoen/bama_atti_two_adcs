#pragma once

#include "cart_state.h"
#include "quaternion.h"

namespace ADCS
{

	struct state_def
	{

		// Data

		quaternion m_q = quaternion(vec3(0.0f, 0.0f, 0.0f), 0.0f);
		vec3 m_ang_vel = vec3::Zero();
		vec4 m_ang_mom_wheels = vec4::Zero();
		

		// Constructor
		state_def() = default;
		state_def(quaternion q, vec3 ang_vel);

		// Methods

		template<typename array_t>
		array_t map_to_array() const
		{
			array_t arr;
			arr[0] = m_q.m_vector[0];
			arr[1] = m_q.m_vector[1];
			arr[2] = m_q.m_vector[2];

			arr[3] = m_q.m_scalar;

			arr[4] = m_ang_vel[0];
			arr[5] = m_ang_vel[1];
			arr[6] = m_ang_vel[2];

			arr[7] = m_ang_mom_wheels[0];
			arr[8] = m_ang_mom_wheels[1];
			arr[9] = m_ang_mom_wheels[2];
			arr[10] = m_ang_mom_wheels[3];

			return arr;
		}

		template<typename array_t>
		static state_def map_from_array(const array_t& arr)
		{
			state_def state;
			state.m_q.m_vector[0] = arr[0];
			state.m_q.m_vector[1] = arr[1];
			state.m_q.m_vector[2] = arr[2];

			state.m_q.m_scalar = arr[3];

			state.m_ang_vel[0] = arr[4];
			state.m_ang_vel[1] = arr[5];
			state.m_ang_vel[2] = arr[6];

			state.m_ang_mom_wheels[0] = arr[7];
			state.m_ang_mom_wheels[1] = arr[8];
			state.m_ang_mom_wheels[2] = arr[9];
			state.m_ang_mom_wheels[3] = arr[10];

			return state;
		}

		template<typename array_t>
		static array_t map_torques_to_array(const vec4& torques)
		{
			array_t arr;
			arr[0] = torques[0];
			arr[1] = torques[1];
			arr[2] = torques[2];
			arr[3] = torques[3];

			return arr;
		}

		template<typename array_t>
		static vec4 map_torques_from_array(const array_t& arr)
		{
			vec4 torques;
			torques[0] = arr[0];
			torques[1] = arr[1];
			torques[2] = arr[2];
			torques[3] = arr[3];

			return torques;
		}



	};
}