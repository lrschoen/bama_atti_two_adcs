#pragma once

#include <array>

#include "face.h"

namespace ADCS
{
	struct css_array
	{
		// Data

		static constexpr int m_num_of_faces = 6;

		std::array<face, m_num_of_faces> m_face_list;

		std::array<vec3, m_num_of_faces> m_normal_vectors;

		Eigen::MatrixXf m_H; // normal vector matrix
		Eigen::VectorXf m_y; // normalized lux value vector

		// Constructor

		css_array();

		// Methods

		vec3 get_sun_vec();
		void find_active_sensors();

	};
}