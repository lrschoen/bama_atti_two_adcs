#pragma once



#include "sensor_suite.h"
#include "actuators.h"
#include "state_def.h"
#include "rk7_control.h"


namespace ADCS
{	
	struct dynamics
	{
        using state_t = std::array<float, 11>;
        using control_t = std::array<float, 4>;

		// Data

		sensor_suite m_sensors;
		actuators m_actuators;

		rk7_control<dynamics> m_rk;

		//static constexpr float m_Ix = 1.18374000e-1f; // kg*m^2
		//static constexpr float m_Iy = 3.86406667e-2f;
		//static constexpr float m_Iz = 9.30666667e-2f;

		static constexpr float m_Ix = 9.30666667e-2f; // kg*m^2
		static constexpr float m_Iy = 9.30666667e-2f;
		static constexpr float m_Iz = 9.30666667e-2f;

        static constexpr float m_integration_dt = 3e-2f;

		std::vector<vec3> m_wheel_axes = std::vector<vec3>(4);

		Eigen::Matrix<float, 3, 3> m_I = Eigen::Matrix<float, 3, 3>::Zero(); // Moment of inertia matrix

		state_def m_current_state;

		quaternion ref_q = quaternion::from_components(0.0f, 0.0f, 0.0f, 1.0f);

        vec4 m_current_torques = vec4::Zero();

		// Constructor
		dynamics();

		// Methods

		
		state_def get_state_derivative(const state_def& state, const vec4& torque_controls);
		state_def step_current_state(const state_def& state, const vec4& torques);
		state_def get_current_posvel(state_def& current_state); // Should modify only q and ang vel
		void spin_up();

        state_t evaluate(const float& /*time*/, const state_t& state);
        state_t evaluate(const float& /*time*/, const state_t& state, const control_t& control);
	};
}