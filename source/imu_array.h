#pragma once

#include "imu.h"

namespace ADCS
{
	struct imu_array
	{
		// Data

		static constexpr int m_num_of_imus = 2;

		std::array<imu, m_num_of_imus> imu_list_m;

		// Constructor

		imu_array() = default;


		// Methods


	};
}
