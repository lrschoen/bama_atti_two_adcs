#include "actuators.h"

using namespace ADCS;

actuators::actuators()
{
	m_normal_vectors[0] = x_axis;
	m_normal_vectors[1] = y_axis;
	m_normal_vectors[2] = z_axis;
	m_normal_vectors[3] = vec3(-1, -1, -1);
	m_normal_vectors[3].normalize();

	for (int i = 0; i < m_num_of_reaction_wheels; i++)
	{
		m_reaction_wheel_list[i].m_normal_vector = m_normal_vectors[i];
	}

	for (int i = 0; i < m_num_of_reaction_wheels; i++)
	{
		m_B.template block<3, 1>(0, i) = m_reaction_wheel_list[i].m_normal_vector;
	}

	m_W = m_B.transpose() * (m_B * m_B.transpose()).inverse();

	for (int i = 0; i < m_num_of_reaction_wheels; i++)
	{
		m_reaction_wheel_list[i].motor.setup(
			m_pwm_names[i],
			m_pwm_pin_names[i],
			m_pwm_pin_numbers[i],
			m_CW_pin_numbers[i],
			m_CCW_pin_numbers[i]
			);
	}
}


vec4 actuators::cart_to_wheel(const vec3& cart)
{
	vec4 wheel = m_W * cart;

	return wheel;
}

vec4 actuators::apply_torques(const vec4& wheel_torques, float dt)
{
	vec4 dpwm_percents = -wheel_torques * dt / m_wheel_MOI / m_motor_max_ang_vel; // should evaluate to a percent of max wheel rpm
	m_current_wheel_pwm_percents += dpwm_percents;

	//if (m_current_wheel_pwm_percents.maxCoeff() > 0.9 || m_current_wheel_pwm_percents.minCoeff() < 0.1) // do pwm range check
	//{
	//	StopMotors();
	//	std::cout << "error: pwm outside of allowed 10% -> 90% range." << std::endl;
	//	std::ofstream file;
	//	file.open("error.txt", std::ios_base::trunc);
	//	file << "error: pwm outside of allowed 10% -> 90% range.";
	//	file.close();
	//	exit(1);
	//}

	set_wheel_speeds(m_current_wheel_pwm_percents);
	return get_current_wheel_ang_mom();
}

void actuators::set_wheel_speeds_and_dirs(const vec4& speeds, const Eigen::Vector4i& dirs)
{
	for (int i = 0; i < m_num_of_reaction_wheels; i++)
	{
		m_reaction_wheel_list[i].set_speed(speeds(i));
		m_reaction_wheel_list[i].set_dir(dirs(i));
	}
}

void actuators::set_wheel_speeds(const vec4& speeds)
{
	for (int i = 0; i < m_num_of_reaction_wheels; i++)
	{
		m_reaction_wheel_list[i].set_speed(speeds(i));
	}
}

void actuators::do_spin_up()
{
	m_current_wheel_pwm_percents = m_spin_up_pwm_percents;
	set_wheel_speeds_and_dirs(m_spin_up_pwm_percents, m_spin_up_dirs);
}

vec4 actuators::get_current_wheel_ang_mom()
{
	return m_current_wheel_pwm_percents * m_wheel_MOI * m_motor_max_ang_vel;
}

void actuators::stop_motors()
{
	for (auto x : m_reaction_wheel_list)
	{
		x.motor.stop();
		x.motor.cleanup();
	}
}